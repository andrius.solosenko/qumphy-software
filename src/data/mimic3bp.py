"""
File: src/data/mimic3bp.py
Project: 22HLT01 QUMPHY
Contact: nando.hegemann@ptb.de
Gitlab: https://gitlab.com/qumphy
Description: Functions handling MIMIC III BP data.
"""

import numpy as np
import src


def load(
    dataset: str | list[str], path: str
) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Load MIMIC III formatted dataset.

    Parameters
    ----------
    dataset : str | list[str]
        Descriptor for parts of the dataset. See examples for details.
    path : str
        Path to dataset files.

    Returns
    -------
    label_data :
        Systolic and diastolic blood pressure values.
    signal_data :
        PPG signal data.
    subject_id_data :
        Subject ids.

    Examples
    --------
    Load a single dataset part

    >>> path = "../data/mimic3bp/formatted/"
    >>> train_l, train_s, train_id = load("train03", path)
    >>> test_l, test_s, test_id = load("test", path)

    Load all training data

    >>> path = "../data/mimic3bp/formatted/"
    >>> train_l, train_s, train_id = load("train", path)

    Load only training data for sets 1, 4 and 10.
    Note that it does not matter if you use `train1` or `train01` as a dataset descriptor.

    >>> path = "../data/mimic3bp/formatted/"
    >>> train_l, train_s, train_id = load(["train1", "train04", "train10"], path)

    Load and combine test and validation data

    >>> path = "../data/mimic3bp/formatted/"
    >>> test_val_l, test_val_s, test_val_id = load(["test", "validate"], path)
    """

    allowed_names = (
        ["test", "validate"]
        + [f"train{n+1:02d}" for n in range(28)]
        + [f"train{n+1}" for n in range(28)]
    )
    if isinstance(dataset, list):  # list of part descriptors
        if not all(fn in allowed_names for fn in dataset):
            idx = np.where([fn not in allowed_names for fn in dataset])[0]
            raise ValueError(f"Unknown datasets: {[dataset[j] for j in idx]}")
        label_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(fn, "label")
            for fn in dataset
        ]
        signal_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(fn, "signal")
            for fn in dataset
        ]
        subject_id_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(fn, "subject_id")
            for fn in dataset
        ]
    elif dataset == "train":  # all training data
        label_file_names = [
            src.misc.pathify(path)
            + src.data.utils.get_filename(dataset + f"{n+1:02d}", "label")
            for n in range(28)
        ]
        signal_file_names = [
            src.misc.pathify(path)
            + src.data.utils.get_filename(dataset + f"{n+1:02d}", "signal")
            for n in range(28)
        ]
        subject_id_file_names = [
            src.misc.pathify(path)
            + src.data.utils.get_filename(dataset + f"{n+1:02d}", "subject_id")
            for n in range(28)
        ]
    else:
        if dataset not in allowed_names:
            raise ValueError(f"Unknown dataset: '{dataset}'")
        label_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(dataset, "label")
        ]
        signal_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(dataset, "signal")
        ]
        subject_id_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(dataset, "subject_id")
        ]

    label_data = np.concatenate([np.load(fn) for fn in label_file_names], axis=0)
    signal_data = np.concatenate([np.load(fn) for fn in signal_file_names], axis=0)
    subject_id_data = np.concatenate(
        [np.load(fn) for fn in subject_id_file_names], axis=0
    )

    return label_data, signal_data, subject_id_data
