"""
File: src/data/deepbeat.py
Project: 22HLT01 QUMPHY
Contact: nando.hegemann@ptb.de
Gitlab: https://gitlab.com/qumphy
Description: Functions handling DeepBeat data.
"""

import numpy as np
import src


def load(
    dataset: str | list[str], path: str
) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """Load DeepBeat formatted dataset.

    Parameters
    ----------
    dataset : str | list[str]
        Descriptor for parts of the dataset. See examples for details.
    path : str
        Path to dataset files.

    Returns
    -------
    label_data :
        Systolic and diastolic blood pressure values.
    signal_data :
        PPG signal data.
    subject_id_data :
        Subject ids.
    quality_data :
        Quality label for the data.

    Examples
    --------
    Load a single dataset part

    >>> path = "../data/deepbeat/formatted/".
    >>> train_l, train_s, train_id, train_q = load("train03", path)
    >>> test_l, test_s, test_id, train_q = load("test", path)

    Load all training and validation data.

    >>> path = "../data/deepbeat/formatted/"
    >>> train_l, train_s, train_id, train_q = load("train", path)
    >>> validate_l, validate_s, validate_id, validate_q = load("validate", path)

    Load only training data for sets 1, 4 and 10.
    Note that it does not matter if you use `train1` or `train01` as a dataset descriptor.

    >>> path = "../data/deepbeat/formatted/"
    >>> train_l, train_s, train_id, train_q = load(["train1", "train04", "train10"], path)

    Load and combine test and validation data.

    >>> path = "../data/deepbeat/formatted/"
    >>> test_val_l, test_val_s, test_val_id, test_val_q = load(
    >>>     ["test", "validate01", "validate02", "validate03"], path
    >>> )
    """

    allowed_names = (
        ["test"]
        + [f"train{n+1:02d}" for n in range(20)]
        + [f"train{n+1}" for n in range(20)]
        + [f"validate{n+1:02d}" for n in range(3)]
        + [f"validate{n+1}" for n in range(3)]
    )
    if isinstance(dataset, list):  # list of part descriptors
        if not all(fn in allowed_names for fn in dataset):
            idx = np.where([fn not in allowed_names for fn in dataset])[0]
            raise ValueError(f"Unknown datasets: {[dataset[j] for j in idx]}")
        label_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(fn, "label")
            for fn in dataset
        ]
        signal_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(fn, "signal")
            for fn in dataset
        ]
        subject_id_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(fn, "subject_id")
            for fn in dataset
        ]
        quality_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(fn, "quality")
            for fn in dataset
        ]
    elif dataset == "train":  # all training data
        label_file_names = [
            src.misc.pathify(path)
            + src.data.utils.get_filename(dataset + f"{n+1:02d}", "label")
            for n in range(20)
        ]
        signal_file_names = [
            src.misc.pathify(path)
            + src.data.utils.get_filename(dataset + f"{n+1:02d}", "signal")
            for n in range(20)
        ]
        subject_id_file_names = [
            src.misc.pathify(path)
            + src.data.utils.get_filename(dataset + f"{n+1:02d}", "subject_id")
            for n in range(20)
        ]
        quality_file_names = [
            src.misc.pathify(path)
            + src.data.utils.get_filename(dataset + f"{n+1:02d}", "quality")
            for n in range(20)
        ]
    elif dataset == "validate":  # all validation data
        label_file_names = [
            src.misc.pathify(path)
            + src.data.utils.get_filename(dataset + f"{n+1:02d}", "label")
            for n in range(3)
        ]
        signal_file_names = [
            src.misc.pathify(path)
            + src.data.utils.get_filename(dataset + f"{n+1:02d}", "signal")
            for n in range(3)
        ]
        subject_id_file_names = [
            src.misc.pathify(path)
            + src.data.utils.get_filename(dataset + f"{n+1:02d}", "subject_id")
            for n in range(3)
        ]
        quality_file_names = [
            src.misc.pathify(path)
            + src.data.utils.get_filename(dataset + f"{n+1:02d}", "quality")
            for n in range(3)
        ]
    else:
        if dataset not in allowed_names:
            raise ValueError(f"Unknown dataset: '{dataset}'")
        label_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(dataset, "label")
        ]
        signal_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(dataset, "signal")
        ]
        subject_id_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(dataset, "subject_id")
        ]
        quality_file_names = [
            src.misc.pathify(path) + src.data.utils.get_filename(dataset, "quality")
        ]

    label_data = np.concatenate([np.load(fn) for fn in label_file_names], axis=0)
    signal_data = np.concatenate([np.load(fn) for fn in signal_file_names], axis=0)
    subject_id_data = np.concatenate(
        [np.load(fn, allow_pickle=True) for fn in subject_id_file_names], axis=0
    )
    quality_data = np.concatenate([np.load(fn) for fn in quality_file_names], axis=0)

    return label_data, signal_data, subject_id_data, quality_data
