"""
File: src/data/mimic4wdb.py
Project: 22HLT01 QUMPHY
Contact: nando.hegemann@ptb.de
Gitlab: https://gitlab.com/qumphy
Description: Functions handling MIMIC IV WDB data.
"""

import numpy as np
import pandas as pd
import src


def load(
    dataset: str | list[str], path: str
) -> tuple[pd.core.frame.DataFrame, np.ndarray, np.ndarray]:
    """Load MIMIC IV formatted dataset.

    Parameters
    ----------
    dataset : str | list[str]
        Descriptor for parts of the dataset. See examples for details.
    path : str
        Path to dataset files.

    Returns
    -------
    numerics_data :
        DataFrame containing numerics data (physiological parameters).
    signal_data :
        PPG data.
    time_data :
        Times (in seconds) of PPG data point measurements.

    Examples
    --------
    Load a single dataset part

    >>> path = "../data/mimic4wdb/training_data"
    >>> train_n, train_s, train_t = load("train03", path)
    >>> test_n, test_s, test_t = load("test", path)
    >>> val_n, val_s, val_t = load("validation", path)

    Load all training data

    >>> path = "../data/mimic4wdb/training_data"
    >>> train_n, train_s, train_t = load("train", path)

    Load only training data for sets 1, 4 and 10.

    >>> path = "../data/mimic4wdb/training_data"
    >>> train_n, train_s, train_t = load(["train01", "train04", "train10"], path)

    Load and combine test and validation data

    >>> path = "../data/mimic4wdb/training_data"
    >>> test_val_n, test_val_s, test_val_t = load(["test", "validation"], path)
    """

    allowed_names = ["test", "validation"] + [f"train{n+1:02d}" for n in range(10)]
    if isinstance(dataset, list):  # list of part descriptors
        if not all(fn in allowed_names for fn in dataset):
            idx = np.where([fn not in allowed_names for fn in dataset])[0]
            raise ValueError(f"Unknown datasets: {[dataset[j] for j in idx]}")
        numerics_file_names = [src.misc.pathify(path) + fn + "_n.csv" for fn in dataset]
        signal_file_names = [src.misc.pathify(path) + fn + "_s.npy" for fn in dataset]
        time_file_names = [src.misc.pathify(path) + fn + "_t.npy" for fn in dataset]
    elif dataset == "train":  # all training data
        numerics_file_names = [
            src.misc.pathify(path) + dataset + f"{n+1:02d}_n.csv" for n in range(10)
        ]
        signal_file_names = [
            src.misc.pathify(path) + dataset + f"{n+1:02d}_s.npy" for n in range(10)
        ]
        time_file_names = [
            src.misc.pathify(path) + dataset + f"{n+1:02d}_t.npy" for n in range(10)
        ]
    else:
        if dataset not in allowed_names:
            raise ValueError(f"Unknown dataset: '{dataset}'")
        numerics_file_names = [src.misc.pathify(path) + dataset + "_n.csv"]
        signal_file_names = [src.misc.pathify(path) + dataset + "_s.npy"]
        time_file_names = [src.misc.pathify(path) + dataset + "_t.npy"]

    numerics_data = pd.concat([pd.read_csv(fn) for fn in numerics_file_names], axis=0)
    signal_data = np.concatenate([np.load(fn) for fn in signal_file_names], axis=0)
    time_data = np.concatenate([np.load(fn) for fn in time_file_names], axis=0)

    return numerics_data, signal_data, time_data
