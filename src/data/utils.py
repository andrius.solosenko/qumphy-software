"""
File: src/data/load.py
Project: 22HLT01 QUMPHY
Contact: nando.hegemann@ptb.de
Gitlab: https://gitlab.com/qumphy
Description: Loading functions for various datasets.
"""


def get_filename(dataset: str, filetype: str) -> str:
    """Create filename for dataset descriptor.

    Parameters
    ----------
    dataset : str
        Dataset descriptor, e.g. ``test``, ``train3`` or ``validate02``.
    filetype : str
        Descriptor for the data part, e.g. ``label`` or ``signal``.

    Returns
    -------
    :
        Filename for the corresponding data.

    Examples
    --------
    >>> get_filename("test", "signal")
    'test_signal.npy'

    >>> get_filename("train3", "label")
    'train_label_03.npy'

    >>> get_filename("train03", "label")
    'train_label_03.npy'
    """

    if not dataset[-1].isdigit():
        return dataset + "_" + filetype + ".npy"
    idx = -1 if not dataset[-2].isdigit() else -2
    return dataset[:idx] + "_" + filetype + f"_{int(dataset[idx:]):02d}" + ".npy"
