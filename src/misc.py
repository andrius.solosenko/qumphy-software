"""
File: src/misc.py
Project: 22HLT01 QUMPHY
Contact: nando.hegemann@ptb.de
Gitlab: https://gitlab.com/qumphy
Description: Miscellaneous functions.
"""

import glob
from typing import Iterator, Sequence

import numpy as np
from torch import tensor
from torch.nn import Module


def extract_file_name(abs_file_name: str) -> str:
    """Extract the file name from an absolute path.

    Parameters
    ----------
    abs_file_name : str
        File name including absolute path to file.

    Returns
    -------
    :
        File name without absolute path.
    """

    return abs_file_name[abs_file_name.rfind("/") + 1 :]


def extract_dir_name(path: str) -> str:
    """Crop the path and return directory name only.

    Parameters
    ----------
    path : str

    Returns
    -------
    :
        Name of the directory without path.
    """

    if path[-1] == "/":
        path = path[:-1]
    return extract_file_name(path)


def get_file_names(path: str, pattern: str = "*.*", abs_path: bool = False) -> list:
    """Return a list of all files in path.

    The list consists of file names only without the path they are located at.

    Parameters
    ----------
    path : str
        Path to get file names from.
    pattern : str, default = '*.*'
        Pattern for glob to filter specific files.
    abs_path : bool, default = False
        If true, return absolute file paths instead of file names.

    Returns
    -------
    :
        List of files and folders in path.
    """

    path = pathify(path)
    abs_names = glob.glob(path + pattern)  # names with absolute path
    names = [extract_file_name(abs_name) for abs_name in abs_names]
    return abs_names if abs_path else names


def pathify(path: str) -> str:
    """Add trailing '/' to path if necessary.

    Parameters
    ----------
    path : str
        Path string.

    Returns
    -------
    :
        Path string.
    """

    return path + "/" if path[-1] != "/" else path


def batch(iterable: Sequence, n: int = 1) -> Iterator:
    """Split iterable into different batches of batchsize n.

    Parameters
    ----------
    iterable : array_like
        Iterable to split.
    n : int, default=1
        Batch size.

    Yields
    ------
    :
        Iterable for different batches.
    """

    for ndx in range(0, len(iterable), n):
        yield iterable[ndx : min(ndx + n, len(iterable))]


def eval_torch_model_by_numpy_ndarray(model: Module, data: np.ndarray) -> np.ndarray:
    """Evaluate a torch model (nn.Module) with a numpy ndarray.

    Parameters
    ----------
    model : nn.Module
        Torch model.
    data : np.ndarray
        Input data.

    Returns
    -------
    np.ndarray
        Model output predictions.
    """
    return model(tensor(data)).detach().numpy()
