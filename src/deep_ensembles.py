"""
File: src/deep_ensembles.py
Project: 22HLT01 QUMPHY
Contact: oskar.pfeffer@ptb.de
Gitlab: https://gitlab.com/qumphy
Description: Deep ensembles.
"""

import numpy as np


def deep_ensemble(
    models: list,
    data: np.ndarray,
    weights: np.ndarray = None,
) -> np.ndarray:
    """Compute deep ensemble of the data using the given models.

    Parameters
    ----------
    models : list
        List of models that must be callable by model(data) and return an np.ndarray.
    data : np.ndarray
        Input data.
    weights : np.ndarray
        Weights for each model.

    Returns
    -------
    np.ndarray
        Weighted model output predictions.

    Examples
    --------
    Compute deep ensemble of three models and weights.
    >>> prediction = deep_ensemble([model0, model1, model2], data, weights)
    For torch models use the helper function `eval_torch_model_by_numpy_ndarray()` from `src/misc.py` and pass the model as `lambda x: eval_torch_model_by_numpy_ndarray(model, x)`.
    >>> data = np.array([[0., 0.], [0., 1.], [1., 0.], [1., 1.]], dtype=np.float32)
    >>> model0 = nn.Linear(2, 1, bias=False)
    >>> model1 = nn.Linear(2, 1, bias=False)
    >>> weights = np.array([.2, .8])
    >>> prediction = deep_ensemble([lambda x: eval_torch_model_by_numpy_ndarray(model0, x), lambda x: eval_torch_model_by_numpy_ndarray(model1, x)], data, weights)
    """
    models_output = np.array([model(data) for model in models])
    return np.average(models_output, axis=0, weights=weights)
