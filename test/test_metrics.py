"""
File: test/test_metrics.py
Project: 22HLT01 QUMPHY
Contact: nando.hegemann@ptb.de
Gitlab: https://gitlab.com/qumphy
Description: Test evaluation metrics.
"""

import numpy as np
import src.metrics as metrics


def test_auc_score_binary() -> None:
    """Test metrics.auc_score_binary()."""

    # test score boundaries
    target = np.concatenate([2 * np.ones((100,)), np.zeros((100,))])
    prediction = np.concatenate([np.zeros((100,)), 2 * np.ones((100,))])
    assert np.abs(metrics.auc_score_binary(target, target) - 1.0) < 1e-12
    assert np.abs(metrics.auc_score_binary(target, prediction) - 0.0) < 1e-12

    # test axis behaviour
    target = np.random.randint(1, 3, (100, 5, 3, 2))
    prediction = np.random.randint(1, 3, (100, 5, 3, 2))
    target_reshape = np.reshape(target, (100, -1))
    prediction_reshape = np.reshape(prediction, (100, -1))
    vals = metrics.auc_score_binary(target, prediction, axis=0)
    expected = np.array(
        [
            metrics.auc_score_binary(t, p)
            for t, p in zip(target_reshape.T, prediction_reshape.T)
        ]
    )
    assert np.linalg.norm(vals.flatten() - expected) <= 1e-12

    # test values
    target = np.array([0, 1, 0, 1, 1, 0, 1, 0, 1, 0])
    prediction = np.array([1, 1, 1, 0, 0, 1, 1, 0, 0, 0])
    assert np.abs(metrics.auc_score_binary(target, prediction) - 0.4) <= 1e-12

    # test shape behaviour
    target = np.random.randint(0, 2, (100, 50, 5, 2))
    assert isinstance(
        metrics.auc_score_binary(target.flatten(), target.flatten()), float
    )
    assert isinstance(metrics.auc_score_binary(target, target), np.ndarray)
    assert metrics.auc_score_binary(target, target, axis=0).shape == (50, 5, 2)
    assert metrics.auc_score_binary(target, target, axis=1).shape == (100, 5, 2)


def test_auc_score_multiclass() -> None:
    """Test metrics.auc_score_multiclass()."""

    # test score boundaries
    target = np.concatenate([np.zeros(100), np.ones(100), 2 * np.ones(100)])
    prediction = np.concatenate(
        [
            np.column_stack([np.ones(100), np.zeros(100), np.zeros(100)]),
            np.column_stack([np.zeros(100), np.ones(100), np.zeros(100)]),
            np.column_stack([np.zeros(100), np.zeros(100), np.ones(100)]),
        ],
        axis=0,
    )
    assert np.abs(metrics.auc_score_multiclass(target, prediction) - 1.0) < 1e-12

    # test axis behaviour
    target = np.random.randint(0, 3, (100, 10, 5, 2))
    prediction = np.random.uniform(0, 1, (100, 3, 10, 5, 2))
    prediction /= np.expand_dims(np.sum(prediction, axis=1), 1)
    target_reshape = np.reshape(target, (100, -1))
    prediction_reshape = np.reshape(prediction, (100, 3, -1))
    vals = metrics.auc_score_multiclass(target, prediction)
    expected = np.array(
        [
            metrics.auc_score_multiclass(t, p)
            for t, p in zip(target_reshape.T, np.moveaxis(prediction_reshape, -1, 0))
        ]
    )
    assert np.linalg.norm(vals.flatten() - expected) <= 1e-12

    # test values
    target = np.array([0, 1, 2, 1, 2, 0])
    prediction = np.array(
        [
            [0.8, 0.1, 0.1],
            [0.2, 0.5, 0.3],
            [0.8, 0.1, 0.1],
            [0.7, 0.2, 0.1],
            [0.4, 0.3, 0.3],
            [0.5, 0.4, 0.1],
        ]
    )
    val = metrics.auc_score_multiclass(target, prediction, comparison_type="ovr")
    assert np.abs(val - 0.6875) <= 1e-12
    val = metrics.auc_score_multiclass(target, prediction, comparison_type="ovo")
    assert np.abs(val - 0.6875) <= 1e-12

    # test shape behaviour
    target = np.random.randint(0, 3, (100, 10, 5, 2))
    prediction = np.random.uniform(0, 1, (100, 3, 10, 5, 2))
    prediction /= np.expand_dims(np.sum(prediction, axis=1), 1)
    assert isinstance(metrics.auc_score_multiclass(target, prediction), np.ndarray)
    assert metrics.auc_score_multiclass(target, prediction).shape == (10, 5, 2)


def test_balanced_accuracy_score() -> None:
    """Test metrics.balanced_accuracy_score()."""
    # test score boundaries
    target = np.concatenate([np.ones((100,)), np.zeros((100,))])
    prediction = np.concatenate([np.zeros((100,)), np.ones((100,))])
    assert np.abs(metrics.balanced_accuracy_score(target, target) - 1.0) < 1e-12
    assert np.abs(metrics.balanced_accuracy_score(target, prediction) - 0.0) < 1e-12

    # test binary classification
    target = np.array([0, 1, 1, 0, 1, 0, 0, 1, 0, 1])
    prediction = np.array([0, 1, 0, 1, 1, 1, 0, 1, 1, 1])
    val = metrics.balanced_accuracy_score(target, prediction)
    assert isinstance(val, float)
    assert val == metrics.balanced_accuracy_score(
        target.reshape(-1, 1), prediction.reshape(-1, 1)
    )
    assert np.linalg.norm(val - 0.6) <= 1e-12

    # test multi-class classification
    target = np.array([1, 2, 2, 2, 1, 2, 1, 0, 1, 1])
    prediction = np.array([1, 1, 2, 0, 0, 1, 1, 0, 0, 2])
    val = metrics.balanced_accuracy_score(target, prediction)
    assert isinstance(val, float)
    assert np.linalg.norm(val - 0.55) <= 1e-12


def test_f1_score() -> None:
    """Test metrics.f1_score()."""

    # test score boundaries
    target = np.concatenate([np.ones((100,)), np.zeros((100,))])
    prediction = np.concatenate([np.zeros((100,)), np.ones((100,))])
    assert np.abs(metrics.f1_score(target, target) - 1.0) < 1e-12
    assert np.abs(metrics.f1_score(target, prediction) - 0.0) < 1e-12

    # test binary classification
    target = np.array([0, 0, 1, 0, 1, 0, 0, 1, 0, 1])
    prediction = np.array([1, 1, 0, 1, 1, 1, 0, 1, 1, 1])
    val = metrics.f1_score(target, prediction)
    assert isinstance(val, float)
    assert val == metrics.f1_score(target, prediction, average="binary")
    assert val == metrics.f1_score(target.reshape(-1, 1), prediction.reshape(-1, 1))
    assert np.linalg.norm(val - 0.5) <= 1e-12

    # test multi-class classification
    target = np.array([1, 2, 2, 2, 1, 2, 1, 0, 1, 1])
    prediction = np.array([1, 1, 2, 0, 0, 1, 1, 0, 0, 2])
    val = metrics.f1_score(target, prediction)
    assert isinstance(val, np.ndarray)
    assert np.all(val == metrics.f1_score(target, prediction, average=None))
    assert np.linalg.norm(val - np.array([0.4, 4 / 9, 1 / 3])) <= 1e-12

    # test multi-lable classification
    target = np.array([[0, 1, 0], [1, 0, 1], [1, 1, 0], [1, 1, 1]])
    prediction = np.array([[1, 0, 1], [1, 1, 1], [0, 0, 1], [1, 1, 0]])
    val = metrics.f1_score(target, prediction)
    assert isinstance(val, np.ndarray)
    assert np.all(val == metrics.f1_score(target, prediction, average=None))
    assert np.linalg.norm(val - np.array([2 / 3, 0.4, 0.4])) <= 1e-12


def test_false_discovery_rate() -> None:
    """Test metrics.false_discovery_rate()."""
    # test score boundaries
    target = np.concatenate([np.ones((100,)), np.zeros((100,))])
    prediction = np.concatenate([np.zeros((100,)), np.ones((100,))])
    assert np.abs(metrics.false_discovery_rate(target, target) - 0.0) < 1e-12
    assert np.abs(metrics.false_discovery_rate(target, prediction) - 1.0) < 1e-12

    # test binary classification
    target = np.array([0, 0, 1, 0, 1, 0, 0, 1, 0, 1])
    prediction = np.array([1, 1, 0, 1, 1, 1, 0, 1, 1, 1])
    val = metrics.false_discovery_rate(target, prediction)
    assert isinstance(val, float)
    assert val == metrics.false_discovery_rate(target, prediction, average="binary")
    assert val == metrics.false_discovery_rate(
        target.reshape(-1, 1), prediction.reshape(-1, 1)
    )
    assert np.linalg.norm(val - 0.625) <= 1e-12

    # test multi-class classification
    target = np.array([1, 2, 2, 2, 1, 2, 1, 0, 1, 1])
    prediction = np.array([1, 1, 2, 0, 0, 1, 1, 0, 0, 2])
    val = metrics.false_discovery_rate(target, prediction)
    assert isinstance(val, np.ndarray)
    assert np.all(val == metrics.false_discovery_rate(target, prediction, average=None))
    assert np.linalg.norm(val - np.array([0.75, 0.5, 0.5])) <= 1e-12

    # test multi-lable classification
    target = np.array([[0, 1, 0], [1, 0, 1], [1, 1, 0], [1, 1, 1]])
    prediction = np.array([[1, 0, 1], [1, 1, 1], [0, 0, 1], [1, 1, 0]])
    val = metrics.false_discovery_rate(target, prediction)
    assert isinstance(val, np.ndarray)
    assert np.all(val == metrics.false_discovery_rate(target, prediction, average=None))
    assert np.linalg.norm(val - np.array([1 / 3, 0.5, 2 / 3])) <= 1e-12


def test_l1_norm() -> None:
    """Test metrics.l1_norm()."""

    # test positive definiteness
    vals = np.random.normal(0, 1, (100, 1))
    assert metrics.l1_norm(vals - vals) <= 1e-12

    # test positivity
    vals = -np.arange(1, 5).reshape(-1, 1)
    assert np.abs(metrics.l1_norm(vals) - 2.5) <= 1e-12

    # test axis behaviour
    vals = np.array([[1, 2], [10, 20]])
    assert np.linalg.norm(metrics.l1_norm(vals, axis=0) - np.array([5.5, 11.0])) < 1e-12
    assert np.linalg.norm(metrics.l1_norm(vals, axis=1) - np.array([1.5, 15.0])) < 1e-12

    # test shape behaviour
    vals = np.random.normal(0, 1, (100, 30, 10, 3))
    assert metrics.l1_norm(vals - vals).shape == (30, 10, 3)
    assert isinstance(metrics.l1_norm(np.arange(1)), float)
    assert isinstance(metrics.l1_norm(np.arange(10)), float)
    assert isinstance(metrics.l1_norm(np.arange(10).reshape(-1, 1, 1)), float)


def test_l2_norm() -> None:
    """Test metrics.l2_norm()."""

    # test positive definiteness
    vals = np.random.normal(0, 1, (100, 1))
    assert metrics.l2_norm(vals - vals) <= 1e-12

    # test positivity
    vals = -np.arange(1, 5).reshape(-1, 1)
    assert (
        np.abs(metrics.l2_norm(vals) - np.sqrt(np.sum(np.arange(1, 5) ** 2 / 4)))
        <= 1e-12
    )

    # test axis behaviour
    vals = np.array([[1, 2], [10, 20]])
    assert (
        np.linalg.norm(metrics.l2_norm(vals, axis=0) - np.sqrt([50.5, 202.0])) < 1e-12
    )
    assert np.linalg.norm(metrics.l2_norm(vals, axis=1) - np.sqrt([2.5, 250.0])) < 1e-12

    # test shape behaviour
    vals = np.random.normal(0, 1, (100, 30, 10, 3))
    assert metrics.l2_norm(vals).shape == (30, 10, 3)
    assert isinstance(metrics.l2_norm(np.arange(1)), float)
    assert isinstance(metrics.l2_norm(np.arange(10)), float)
    assert isinstance(metrics.l2_norm(np.arange(10).reshape(-1, 1, 1)), float)


def test_matthews_correlation_coefficient() -> None:
    """Test metrics.matthews_correlation_coefficient()."""

    # test score boundaries
    target = np.concatenate([np.ones((100,)), np.zeros((100,))])
    prediction = np.concatenate([np.zeros((100,)), np.ones((100,))])
    assert (
        np.abs(metrics.matthews_correlation_coefficient(target, target) - 1.0) < 1e-12
    )
    assert (
        np.abs(metrics.matthews_correlation_coefficient(target, prediction) + 1.0)
        < 1e-12
    )

    # test binary classification
    target = np.array([0, 0, 1, 0, 1, 0, 0, 1, 0, 1])
    prediction = np.array([1, 1, 0, 1, 1, 1, 0, 1, 1, 1])
    val = metrics.matthews_correlation_coefficient(target, prediction)
    assert isinstance(val, float)
    assert val == metrics.matthews_correlation_coefficient(
        target.reshape(-1, 1), prediction.reshape(-1, 1)
    )
    assert np.linalg.norm(val + 0.10206207261596577) <= 1e-12

    # test multi-class classification
    target = np.array([1, 2, 2, 2, 1, 2, 1, 0, 1, 1])
    prediction = np.array([1, 1, 2, 0, 0, 1, 1, 0, 0, 2])
    val = metrics.matthews_correlation_coefficient(target, prediction)
    assert isinstance(val, float)
    assert np.linalg.norm(val - 0.13130643285972254) <= 1e-12


def test_mean_absolute_error() -> None:
    """Test metrics.mean_absolute_error()."""

    # test positive definiteness
    target = np.random.normal(0, 1, (100, 1))
    assert metrics.mean_absolute_error(target, target) <= 1e-12

    # test positivity
    target = np.random.normal(0, 1, (100, 1))
    prediction = np.random.normal(0, 1, (100, 1))
    assert (
        np.abs(
            metrics.mean_absolute_error(target, prediction)
            - metrics.mean_absolute_error(prediction, target)
        )
        <= 1e-12
    )

    # test axis behaviour
    target = np.array([[1, 2], [10, 20]])
    prediction = np.zeros((2, 2))
    assert (
        np.linalg.norm(
            metrics.mean_absolute_error(target, prediction, axis=0)
            - np.array([5.5, 11.0])
        )
        < 1e-12
    )
    assert (
        np.linalg.norm(
            metrics.mean_absolute_error(target, prediction, axis=1)
            - np.array([1.5, 15.0])
        )
        < 1e-12
    )

    # test shape behaviour
    target = np.random.normal(0, 1, (100, 30, 10, 3))
    assert metrics.mean_absolute_error(target, target).shape == (30, 10, 3)
    assert isinstance(metrics.mean_absolute_error(np.arange(1), np.arange(1)), float)
    assert isinstance(metrics.mean_absolute_error(np.arange(10), np.arange(10)), float)
    assert isinstance(
        metrics.mean_absolute_error(
            np.arange(10).reshape(-1, 1, 1), np.arange(10).reshape(-1, 1, 1)
        ),
        float,
    )


def test_precision_score() -> None:
    """Test metrics.precision_score()."""
    # test score boundaries
    target = np.concatenate([np.ones((100,)), np.zeros((100,))])
    prediction = np.concatenate([np.zeros((100,)), np.ones((100,))])
    assert np.abs(metrics.precision_score(target, target) - 1.0) < 1e-12
    assert np.abs(metrics.precision_score(target, prediction) - 0.0) < 1e-12

    # test binary classification
    target = np.array([0, 0, 1, 0, 1, 0, 0, 1, 0, 1])
    prediction = np.array([1, 1, 0, 1, 1, 1, 0, 1, 1, 1])
    val = metrics.precision_score(target, prediction)
    assert isinstance(val, float)
    assert val == metrics.precision_score(target, prediction, average="binary")
    assert val == metrics.precision_score(
        target.reshape(-1, 1), prediction.reshape(-1, 1)
    )
    assert np.linalg.norm(val - 0.375) <= 1e-12

    # test multi-class classification
    target = np.array([1, 2, 2, 2, 1, 2, 1, 0, 1, 1])
    prediction = np.array([1, 1, 2, 0, 0, 1, 1, 0, 0, 2])
    val = metrics.precision_score(target, prediction)
    assert isinstance(val, np.ndarray)
    assert np.all(val == metrics.precision_score(target, prediction, average=None))
    assert np.linalg.norm(val - np.array([0.25, 0.5, 0.5])) <= 1e-12

    # test multi-lable classification
    target = np.array([[0, 1, 0], [1, 0, 1], [1, 1, 0], [1, 1, 1]])
    prediction = np.array([[1, 0, 1], [1, 1, 1], [0, 0, 1], [1, 1, 0]])
    val = metrics.precision_score(target, prediction)
    assert isinstance(val, np.ndarray)
    assert np.all(val == metrics.precision_score(target, prediction, average=None))
    assert np.linalg.norm(val - np.array([2 / 3, 0.5, 1 / 3])) <= 1e-12


def test_root_mean_square_error() -> None:
    """Test metrics.root_mean_square_error()."""

    # test positive definiteness
    target = np.random.normal(0, 1, (100, 1))
    assert metrics.root_mean_square_error(target, target) <= 1e-12

    # test positivity
    target = np.random.normal(0, 1, (100, 1))
    prediction = np.random.normal(0, 1, (100, 1))
    assert (
        np.abs(
            metrics.root_mean_square_error(target, prediction)
            - metrics.root_mean_square_error(prediction, target)
        )
        <= 1e-12
    )

    # test axis behaviour
    target = np.array([[1, 2], [10, 20]])
    prediction = np.zeros((2, 2))
    assert (
        np.linalg.norm(
            metrics.root_mean_square_error(target, prediction, axis=0)
            - np.sqrt([50.5, 202.0])
        )
        < 1e-12
    )
    assert (
        np.linalg.norm(
            metrics.root_mean_square_error(target, prediction, axis=1)
            - np.sqrt([2.5, 250.0])
        )
        < 1e-12
    )

    # test shape behaviour
    target = np.random.normal(0, 1, (100, 30, 10, 3))
    assert metrics.root_mean_square_error(target, target).shape == (30, 10, 3)
    assert isinstance(metrics.root_mean_square_error(np.arange(1), np.arange(1)), float)
    assert isinstance(
        metrics.root_mean_square_error(np.arange(10), np.arange(10)), float
    )
    assert isinstance(
        metrics.root_mean_square_error(
            np.arange(10).reshape(-1, 1, 1), np.arange(10).reshape(-1, 1, 1)
        ),
        float,
    )


if __name__ == "__main__":
    print("This script is not meant to be executed as main. Run via pytest.")
