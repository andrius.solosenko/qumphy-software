"""
File: src/deep_ensembles.py
Project: 22HLT01 QUMPHY
Contact: oskar.pfeffer@ptb.de
Gitlab: https://gitlab.com/qumphy
Description: Test deep ensembles.
"""

import numpy as np

import torch.nn as nn
from src.deep_ensembles import deep_ensemble
from src.misc import eval_torch_model_by_numpy_ndarray as eval_torch

# TODO(Nando): Don't import torch or other project modules in tests, use mocks instead.


def test_deep_ensembles() -> None:
    """Test deep_ensemble()."""

    # test single label
    model0 = nn.Linear(2, 1, bias=False)
    model1 = nn.Linear(2, 1, bias=False)
    nn.init.zeros_(model0.weight)
    nn.init.ones_(model1.weight)
    data = np.array([[0.0, 0.0], [0.0, 1.0], [1.0, 0.0], [1.0, 1.0]], dtype=np.float32)
    expected = np.array([[0.0], [0.5], [0.5], [1.0]], dtype=np.float32)
    prediction = deep_ensemble(
        [lambda x: eval_torch(model0, x), lambda x: eval_torch(model1, x)], data
    )
    weighted_prediction = deep_ensemble(
        [lambda x: eval_torch(model0, x), lambda x: eval_torch(model1, x)],
        data,
        weights=np.array([0.0, 1.0]),
    )
    assert prediction.shape == (4, 1)
    assert (prediction == expected).all()
    assert (weighted_prediction == expected * 2).all()
    assert isinstance(prediction, np.ndarray)

    # test multiple labels
    model1 = nn.Linear(3, 3, bias=False)
    model0 = nn.Linear(3, 3, bias=False)
    nn.init.zeros_(model0.weight)
    nn.init.ones_(model1.weight)
    data = np.array(
        [
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 1.0],
            [0.0, 1.0, 0.0],
            [0.0, 1.0, 1.0],
            [1.0, 0.0, 0.0],
            [1.0, 0.0, 1.0],
            [1.0, 1.0, 0.0],
            [1.0, 1.0, 1.0],
        ],
        dtype=np.float32,
    )
    prediction = deep_ensemble(
        [lambda x: eval_torch(model0, x), lambda x: eval_torch(model1, x)], data
    )
    expected = np.array(
        [
            [0.0, 0.0, 0.0],
            [0.5, 0.5, 0.5],
            [0.5, 0.5, 0.5],
            [1.0, 1.0, 1.0],
            [0.5, 0.5, 0.5],
            [1.0, 1.0, 1.0],
            [1.0, 1.0, 1.0],
            [1.5, 1.5, 1.5],
        ],
        dtype=np.float32,
    )
    weighted_prediction = deep_ensemble(
        [lambda x: eval_torch(model0, x), lambda x: eval_torch(model1, x)],
        data,
        weights=np.array([0.0, 1.0]),
    )

    assert (prediction == expected).all()
    assert prediction.shape == (8, 3)
    assert (weighted_prediction == expected * 2).all()

    # test non-torch model
    data = np.array([[0.0, 0.0], [0.0, 1.0], [1.0, 0.0], [1.0, 1.0]], dtype=np.float32)

    def model(x):
        return x

    assert (deep_ensemble([model, model], data) == data).all()

    # test dtype=np.float64
    data = np.array([[0.0, 0.0], [0.0, 1.0], [1.0, 0.0], [1.0, 1.0]], dtype=np.float64)
    assert (deep_ensemble([model, model], data) == data).all()
