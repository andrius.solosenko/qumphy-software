![QUMPHY Logo Full](logo/qumphy_textlogo_color.png)

# 22HLT01 QUMPHY Software Repository

## Project Website

You can find any information about the 22HLT01 QUMPHY project on our project website: [https://www.qumphy.ptb.de/home](https://www.qumphy.ptb.de/home)

## Getting started

> [note]
> You can exchange `mamba` by `conda` in all of the commands if you don't use mamba.

Install a working conda environment from the .yml file in this repo using:
```
mamba env create -f environment.yml
```
activate the environment with
```
mamba activate qumphy
```
and add the repository to the conda/mamba environment path:
```
mamba develop .
```
This way, the path to this repository is only set if the `qumphy` environment is active.
You can list installed path by looking into the site-packages `conda.pth` file, e.g.:
```
cat ~/anaconda3/envs/qumphy/lib/python3.9/site-packages/conda.pth
```
Then use `mamba deactivate` to deactivate the environment and activate it again to start working.

## Code Auto-Doc

You can find the documentation here: [https://qumphy-software.readthedocs.io/en/latest/](https://qumphy-software.readthedocs.io/en/latest/).
You can also create the software documentation yourself by running
```
mamba activate qumphy
cd doc
make html
```
To view the documentation locally, open `doc/build/html/index.html` in any browser.

## Meta Documentation

To track performance of models and make intercomparison of different approaches feasible, there exist some information files in the `info/` directory.
The table below lists the files and what they are about.

| file | content |
| --- | --- |
| <a href="./info/eval_metrics.md" target="_blank">info/eval_metrics.md</a> | comparison of performance metrics for WP1 models |
| <a href="./app/README.md" target="_blank">app/README.md</a> | description of user-ready application scripts |

## Funding

This project (22HTL01 QUMPHY) has received funding from the EMPIR programme cofinanced by the Participating States and from the European Union’s Horizon 2020 research and innovation programme.
