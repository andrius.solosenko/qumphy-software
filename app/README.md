# Overview

| script                            | description |
| --------------------------------- | --- |
| `deepbeat_converter.py`           | Split DeepBeat dataset into multiple `.npy` files for easier handling of data. The splits are stratified in a way to not distribute data of a single patient over more then one stride. |
| `deepbeat_data_visualization.py`  | Create `.png` files with random samples of PPG signals for each data file of the (converted) DeepBeat dataset. |
| `mimic3bp_converter.py`           | Split MIMIC III BP dataset into multiple `.npy` files for easier handling of data. The splits are stratified in a way to not distribute data of a single patient over more then one stride. |
| `mimic3bp_data_visualization.py`  | Create `.png` files with random samples of PPG signals for each data file of the (converted) MIMIC III BP dataset. |
| `tutorial_metrics.py`             | Tutorial script to show the use of evaluation metrics. |