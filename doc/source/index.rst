.. image:: ../../logo/qumphy_textlogo_color.png
   :alt: logo

|

22HLT01 QUMPHY Software Package
===============================

For more information, see the |22HLT01 QUMPHY Project Homepage| and the |QUMPHY Software Package Repository|.

.. |22HLT01 QUMPHY Project Homepage| raw:: html

    <a href="https://www.qumphy.ptb.de/home" target="_blank">22HLT01 QUMPHY Project Homepage</a>

.. |QUMPHY Software Package Repository| raw:: html

    <a href="https://gitlab.com/qumphy/qumphy-software" target="_blank">Software Package Repository</a>

License
=======

This work is licensed under European Union Public License v1.2 or later.

``SPDX-License-Identifier: EUPL-1.2``

Funding
=======

The development of the 22HLT01 QUMPHY Software Package has been funded by the |EPM project 22HLT01 QUMPHY|.
The project (22HLT01 QUMPHY) has received funding from the European Partnership on Metrology, co-financed from the European Union’s Horizon Europe Research and Innovation Programme and by the Participating States.

.. |EPM project 22HLT01 QUMPHY| raw:: html

    <a href="https://www.qumphy.ptb.de/home/" target="_blank">EPM project 22HLT01 QUMPHY</a>

Contents
========

.. toctree::
   :maxdepth: 2

   app
   src


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
