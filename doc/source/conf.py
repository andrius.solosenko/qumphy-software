# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join("..", "..", "app")))
sys.path.insert(0, os.path.abspath(os.path.join("..", "..", "src")))

# -- Mockups for standard python packages ------------------------------------

# Python packages that are not accessible in the standard python version need
# to be imported during the generation of the sphinx-doc. However, the code
# is never executed. Hence it suffices to mock these modules.
# NOTE: This handles the autodoc warnings for unknown modules.
autodoc_mock_imports = [
    "numpy" "scipy" "pandas" "h5py" "tqdm" "sklearn" "sklearn.metrics"
]

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "22HLT01 QUMPHY Software Package"
copyright = "2023, Nando Hegemann"
author = "Nando Hegemann"
release = "01.07.2023"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx_autodoc_typehints",  # needs: pip install sphinx-autodoc-typehints
    "sphinx.ext.napoleon",  # html doc theme
    "sphinx.ext.todo",
    "sphinx.ext.coverage",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
]

templates_path = ["_templates"]
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_static_path = []
