# Evaluation and Performance Metrics

## Table of Contents
1. [Leaderboard Deepbeat](#leaderboard-deepbeat---performance-comparison-results)
2. [Leaderboard MIMIC III BP](#leaderboard-mimic-iii-bp---performance-comparison-results)
3. [How to create comparable performance evaluations](#how-to-create-comparable-performance-evaluations)
4. [How to run the code for performance evaluation](#how-to-run-the-code-for-performance-evaluation)
5. [Model Meta Information](#model-meta-information)

## Leaderboard Deepbeat - Performance Comparison Results

| model             | Contributor | AUC   | F1    | Acc   | Model Info      |
| ----------------- | ----------- | ----- | ----- | ----- | --------------- |
| xresnet1d50       | UOL (NS)    | 0.89  | 0.65  | 0.77  | [DB-1](#db-1)   | 
| S4                | UOL (NS)    | 0.919 | 0.70  | 0.828 | [DB-2](#db-2)   |

## Leaderboard MIMIC III BP - Performance Comparison Results

| model             | Contributor | $L^1$-error (MAE) - sys / dia | $L^2$-error (RMSE) - sys / dia | Model Info        |
| ----------------- | ----------- | ----------------------------- | ------------------------------ | ----------------- |
| xresnet1d50       | UOL (NS)    | 15.2 / 8.2                    |                                | [M3-1](#m3-1)     |

## How to create comparable performance evaluations

To create uncertainty evaluations and compare them in a meaningful way to the other models, use the respective functions in the `metrics` module.
As an example you can use the `metrics.mean_absolute_error` to compute the MAE for a regression problem or `metrics.f1_score` to compute the F1 score for a classification problem.
All of the evaluation metrics follow the same input structure.

**For Python users:**
Simply plug the test output dataset and the output of your model of the test input dataset into the metrics you desire.

**For non-Python users:**
To compute the metric of your choice, you simply need to evaluate the output of your model on the test input dataset and save the output into a common file format.
Then you can simply load the test output dataset and your model evaluations into a python script and call all the metrics suitable to the problem.
You can use `app/tutorial_metrics.py` as a basis to code from.

For more details see or run `app/tutorial_metrics.py` or check out the <a href="https://qumphy-software.readthedocs.io/en/latest/src.html#module-src.metrics" target="_blank">metrics documentation</a>.

## Model Meta Information

### DB-1

| key        | value |
| ---------- | --- |
| dataset    | DeepBeat |
| model      | xresnet1d50 |
| script     | <a href="https://gitlab.com/qumphy/wp1-raw-time-series/-/blob/main/main_ppg.py" target="_blank">gitlab.com/qumphy/wp1-raw-time-series/main_ppg.py</a> |
| commit SHA | 80c83ff07b39c07ff19b72a99d47fe15c7a2fdc1 |
| command    | `python main_ppg.py --data ./deepbeat/memmap --input-size 200 --architecture xresnet1d50 --finetune-dataset deepbeat ` |
| comment    | See <a href="https://gitlab.com/qumphy/wp1-raw-time-series/-/blob/main/README.md" target="_blank">gitlab.com/qumphy/wp1-raw-time-series/README.md</a> for more info. |

### DB-2

| key        | value |
| ---------- | --- |
| dataset    | DeepBeat |
| model      | xresnet1d50 |
| script     | <a href="https://gitlab.com/qumphy/wp1-raw-time-series/-/blob/main/main_ppg.py" target="_blank">gitlab.com/qumphy/wp1-raw-time-series/main_ppg.py</a> |
| commit SHA | 80c83ff07b39c07ff19b72a99d47fe15c7a2fdc1 |
| command    | `python main_ppg.py --data ./deepbeat/memmap --input-size 800 --architecture s4 --precision 32 --s4-n 8 --s4-h 512 --batch-size 32` |
| comment    | See <a href="https://gitlab.com/qumphy/wp1-raw-time-series/-/blob/main/README.md" target="_blank">gitlab.com/qumphy/wp1-raw-time-series/README.md</a> for more info. |

### M3-1

| key        | value |
| ---------- | --- |
| dataset    | MIMIC III DB |
| model      | xresnet1d50 |
| script     | <a href="https://gitlab.com/qumphy/wp1-raw-time-series/-/blob/main/main_ppg.py" target="_blank">gitlab.com/qumphy/wp1-raw-time-series/main_ppg.py</a> |
| commit SHA | 80c83ff07b39c07ff19b72a99d47fe15c7a2fdc1 |
| command    | `python main_ppg.py --data ./mimiciiibp/memmap --input-size 200 --architecture xresnet1d50 --finetune-dataset mimiciiibp` |
| comment    | See <a href="https://gitlab.com/qumphy/wp1-raw-time-series/-/blob/main/README.md" target="_blank">gitlab.com/qumphy/wp1-raw-time-series/README.md</a> for more info. |