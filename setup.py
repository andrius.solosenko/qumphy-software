import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="qumphy-software",
    version="0.0.1",
    author="22HLT01 QUMPHY",
    author_email="nando.hegemann@ptb.de",
    description=(
        "Toolbox for uncertainty quantification of machine learning models "
        + "applied to photoplethysmography signals."
    ),
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/qumphy/qumphy-software",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "numpy>=1.20.0",
        "sphinx-autodoc-typehints>=1.18.1",
    ],
)
